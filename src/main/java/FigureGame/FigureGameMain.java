package FigureGame;

import FigureGame.List.FigureList;
import FigureGame.Unit.Circle;
import FigureGame.Unit.Figure;
import FigureGame.Unit.Rectangle;
import FigureGame.Unit.Square;

public class FigureGameMain {
    public static void main(String[] args) {


        //ПРОСТО ДЕМОНСТРАЦІЯ

        Circle circle = new Circle(5.5);
        Rectangle rectangle = new Rectangle(10, 20);
        Square square = new Square(5);

        System.out.printf("CircleArea = %.2f\n" +
                        "RectangleArea = %.2f\n" +
                        "SquareArea = %.2f\n",
                circle.findArea(),
                rectangle.findArea(),
                square.findArea());

        FigureList figures = new FigureList(new Figure[]{circle, rectangle, square});
        System.out.println("Sum of Areas = " + figures.findSummArea());
    }
}

package FigureGame.List;

import FigureGame.Unit.Figure;

public class FigureList {
    private final Figure[] list;

    public FigureList(Figure[] list) {
        this.list = list;
    }


    public double findSummArea() {
        double sum = 0;
        for (Figure figure : list) {
            sum += figure.findArea();
        }
        return sum;
    }
}

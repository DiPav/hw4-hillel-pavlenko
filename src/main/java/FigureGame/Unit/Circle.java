package FigureGame.Unit;

public class Circle implements Figure {
    public static final double PI = 3.14;
    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double findArea() {
        return PI * (radius*radius);
    }
}

package FigureGame.Unit;

public interface Figure {

    double findArea();
}

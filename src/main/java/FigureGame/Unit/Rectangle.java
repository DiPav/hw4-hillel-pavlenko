package FigureGame.Unit;

public class Rectangle implements Figure {
    private final double width;
    private final double height;


    public Rectangle(double width, double high) {
        this.width = width;
        this.height = high;
    }


    @Override
    public double findArea() {
        return (width * height) / 2;
    }
}

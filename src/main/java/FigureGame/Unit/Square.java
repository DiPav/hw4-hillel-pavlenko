package FigureGame.Unit;

public class Square implements Figure {
    private final double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double findArea() {
        return side*side;
    }
}

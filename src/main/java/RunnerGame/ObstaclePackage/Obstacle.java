package RunnerGame.ObstaclePackage;

import RunnerGame.PlayerPackage.Player;

public abstract class Obstacle {
        public abstract boolean overcome(Player player);
}

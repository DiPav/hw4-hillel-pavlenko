package RunnerGame.ObstaclePackage;

import RunnerGame.PlayerPackage.Player;

public class Treadmill extends Obstacle{
    private final double distance;

    public Treadmill(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Treadmill{" +
                distance +
                "m}";
    }

    @Override
    public boolean overcome(Player player) {
        return player.getDistanceStamina() >= distance;
    }
}

package RunnerGame.ObstaclePackage;

import RunnerGame.PlayerPackage.Player;

public class Wall extends Obstacle {
    private final double height;

    public Wall(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Wall{" +
                height +
                "m}";
    }

    @Override
    public boolean overcome(Player player) {
        return player.getMaxJumpHeight() >= height;
    }
}

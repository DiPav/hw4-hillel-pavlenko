package RunnerGame.PlayerPackage;

public abstract class Player {
    final double distanceStamina;
    final double maxJumpHeight;
    final String name;

    Player(String name, double distanceStamina, double maxJumpHeight) {
        this.distanceStamina = distanceStamina;
        this.maxJumpHeight = maxJumpHeight;
        this.name = name;
    }

    public double getDistanceStamina() {
        return distanceStamina;
    }

    public double getMaxJumpHeight() {
        return maxJumpHeight;
    }

    public String getName() {
        return name;
    }

    public void run() {
        System.out.printf("%s is running!", name);
    }

    public void jump() {
        System.out.printf("%s is jumping!", name);
    }

}

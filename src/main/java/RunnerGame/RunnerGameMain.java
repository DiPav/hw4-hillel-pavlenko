package RunnerGame;

import RunnerGame.ObstaclePackage.Obstacle;
import RunnerGame.ObstaclePackage.Treadmill;
import RunnerGame.ObstaclePackage.Wall;
import RunnerGame.PlayerPackage.Cat;
import RunnerGame.PlayerPackage.Human;
import RunnerGame.PlayerPackage.Player;
import RunnerGame.PlayerPackage.Robot;
import RunnerGame.Service.Game;
import RunnerGame.Service.Ui;

public class RunnerGameMain {
    public static void main(String[] args) {


        //ПРОСТО ДЕМОНСТРАЦІЯ

        Wall wall1 = new Wall(5.2);
        Treadmill treadmill = new Treadmill(10);
        Wall wall2 = new Wall(7.8);

        Robot robot = new Robot("R2D2", 20, 2);
        Human human = new Human("Oleg", 10, 10);
        Cat cat = new Cat("Murchik", 2, 20);

        Ui ui = new Ui();
        Obstacle[] obstacles = new Obstacle[]{wall1, treadmill, wall2};
        Player[] players = new Player[]{robot, cat, human};
        Game game = new Game(obstacles, players, ui);
        game.run();
    }
}

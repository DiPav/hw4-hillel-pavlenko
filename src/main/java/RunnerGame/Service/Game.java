package RunnerGame.Service;

import RunnerGame.ObstaclePackage.Obstacle;
import RunnerGame.PlayerPackage.Player;

public class Game {
    Obstacle[] obstacles;
    Player[] players;
    Ui ui;

    public Game(Obstacle[] obstacles, Player[] players, Ui ui) {
        this.obstacles = obstacles;
        this.players = players;
        this.ui = ui;
    }

    public void run() {
        ui.startMessage(obstacles.length,players);
        for (Player player : players) {
            ui.visualLine();
            for (int j = 0; j < obstacles.length; j++) {
                if (obstacles[j].overcome(player)) {
                    ui.successMessage(player, obstacles[j], j);
                } else {
                    ui.failMessage(player, obstacles[j], j);
                    break;
                }
            }
        }

    }
}

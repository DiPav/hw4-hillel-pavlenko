package RunnerGame.Service;

import RunnerGame.ObstaclePackage.Obstacle;
import RunnerGame.ObstaclePackage.Treadmill;
import RunnerGame.ObstaclePackage.Wall;
import RunnerGame.PlayerPackage.Player;

public class Ui {

    public void successMessage(Player player, Obstacle obstacle, int numberOfObstacle) {
        if (obstacle instanceof Treadmill) player.run();
        else player.jump();
        System.out.printf(" Player %s overcame an obstacle %s on distance %d\n", player.getName(), obstacle.toString(), numberOfObstacle + 1);
    }

    public void failMessage(Player player, Obstacle obstacle, int numberOfObstacle) {
        if (obstacle instanceof Treadmill) player.run();
        else player.jump();
        System.out.printf(" Player %s failed on obstacle %s on distance %d\n", player.getName(), obstacle.toString(), numberOfObstacle + 1);
    }

    public void visualLine() {
        System.out.println("------------------------------------");
    }

    public void startMessage(int numberOfObstacles, Player[] players) {
        visualLine();
        System.out.printf("We have %d obstacles and %d players! Here are the list of participants:\n",numberOfObstacles,players.length);
        for (Player player : players) {
            System.out.println(player.getName());
        }
    }
}

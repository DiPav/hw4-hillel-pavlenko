package FigureGame.List;

import FigureGame.Unit.Circle;
import FigureGame.Unit.Figure;
import FigureGame.Unit.Rectangle;
import FigureGame.Unit.Square;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FigureListTest {

    @Test
    void ShouldGiveSummArea() {
        Circle circle = new Circle(5.5);
        Rectangle rectangle = new Rectangle(10, 20);
        Square square = new Square(5);

        FigureList figures = new FigureList(new Figure[]{circle, rectangle, square});

        assertEquals(219.985, figures.findSummArea());
    }
    @Test
    void ShouldReturnZero(){
        FigureList figures = new FigureList(new Figure[]{});
        assertEquals(0,figures.findSummArea());
    }
}
package RunnerGame.ObstaclePackage;

import RunnerGame.PlayerPackage.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreadmillTest {

    @Test
    void ShouldGiveDistance() {
        Treadmill treadmill = new Treadmill(10);
        assertEquals(10,treadmill.getDistance());
    }

    @Test
    void ShouldReturnTrue() {
        Human human = new Human("Oleg", 13, 10);
        Treadmill treadmill = new Treadmill(10);
        assertTrue(treadmill.overcome(human));
    }
    @Test
    void ShouldReturnFalse() {
        Human human = new Human("Oleg", 6, 10);
        Treadmill treadmill = new Treadmill(10);
        assertFalse(treadmill.overcome(human));
    }
}
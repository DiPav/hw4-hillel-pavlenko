package RunnerGame.ObstaclePackage;

import RunnerGame.PlayerPackage.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WallTest {

    @Test
    void ShouldGiveHeight() {
        Wall wall = new Wall(10);
        assertEquals(10,wall.getHeight());
    }

    @Test
    void ShouldReturnTrue() {
        Human human = new Human("Oleg", 10, 15);
        Wall wall = new Wall(10);
        assertTrue(wall.overcome(human));
    }
    @Test
    void ShouldReturnFalse() {
        Human human = new Human("Oleg", 10, 6);
        Wall wall = new Wall(10);
        assertTrue(wall.overcome(human));
    }
}